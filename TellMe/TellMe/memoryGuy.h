/****************************************************************************
 *                                                                          *
 * File    : memoryGuy.h                                                    *
 *                                                                          *
 * Purpose : Memory Related Functions                                       *
 *                                                                          *
 * History : Date      Part of TellMe                                       *
 *           03/3/2012 Subhajit Sahu                                        *
 *                                                                          *
 ****************************************************************************/


#ifndef	_memoryGuy_h_
#define	_memoryGuy_h_		1



// ----Type Abbreviations--------------------------------------------
#ifndef		CHAR
#define		CHAR			char
#define		LPCHAR			CHAR*
#endif
#ifndef		BYTE
#define		BYTE			unsigned CHAR
#define		LPBYTE			BYTE*
#endif
#ifndef		SHORT
#define		SHORT			short
#define		LPSHORT			SHORT*
#endif
#ifndef		WORD
#define		WORD			unsigned SHORT
#define		LPWORD			WORD*
#endif
#ifndef		INT
#define		INT				int
#define		LPINT			INT*
#endif
#ifndef		UINT
#define		UINT			unsigned INT
#define		LPUINT			UINT*
#endif
#ifndef		LONG
#define		LONG			long
#define		LPLONG			LONG*
#endif
#ifndef		DWORD
#define		DWORD			unsigned LONG
#define		LPDWORD			DWORD*
#endif
#ifndef		VERYLONG
#define		VERYLONG		__int64
#define		LPVERYLONG		VERYLONG*
#endif
#ifndef		QWORD
#define		QWORD			unsigned VERYLONG
#define		LPQWORD			QWORD*
#endif





// Double DWORD
typedef	struct _DWORD2
	{
	DWORD	low;
	DWORD	high;
	}DWORD2;

// Double int
typedef	struct _INT2
	{
	int		low;
	int		high;
	}INT2;



// Data Queue [Circular and expandable] (for any datatype)
#define	TM_DATA_QUEUE(Datatype)	\
	UINT		Size;\
	UINT		Number;\
	UINT		Front;\
	UINT		Rear;\
	UINT		BufferingLength;\
	Datatype*	Data;

// Data List [expandable] (for any datatype)
#define	TM_DATA_LIST(Datatype)	\
	UINT		Size;\
	UINT		Number;\
	UINT		BufferingLength;\
	Datatype*	Data;

// Data Stack [expandable] (for any data type)
#define	TM_DATA_STACK(Datatype)	\
	UINT		Buffer;\
	UINT		Size;\
	UINT		Top;\
	UINT		BufferingLength;\
	Datatype*	Data;

// Data Link List (for any data type)
#define	TM_DATA_LINK_LIST(Structtype, Datatype)	\
	Structtype*	Next;\
	Datatype	Data;
	



// ----Memory Related Macros-----------------------------------------


// _TS(size)
// Give the size in bytes required for text of given size
#define	_AS(size)		size
#define	_WS(size)		((size)<<1)
#ifdef	UNICODE
	#define	_TS				_WS
#else
	#define	_TS				_AS
#endif
#define	TEXT_SIZE			_TS


// Allocate Memory
#define tmMemAlloc(size)	\
	GlobalAlloc(GMEM_FIXED, size)

// Allocate Memory for Array
#define	tmArrayAlloc(type, num)	\
	(type*) tmMemAlloc(num * sizeof(type))

// Allocate Memory for ANSI String
#define	tmStrAllocA(len)	\
	(char*) tmMemAlloc(_AS(len))

// Allocate Memory for UNICODE String
#define	tmStrAllocW(len)	\
	(WCHAR*) tmMemAlloc(_WS(len))
	
// Allocate Memory for Text
#define	tmStrAlloc(len)	\
	(TCHAR*) tmMemAlloc(_TS(len))

// Allocate Memory for a Structure
#define tmStructAlloc(type)	\
	(type*) tmMemAlloc(sizeof(type))

// Allocate & Initialize Memory
#define	tmMemInitAlloc(size)	\
	GlobalAlloc(GMEM_FIXED | GMEM_ZEROINIT, size)

// Allocate & Initialize Memory for Array
#define	tmArrayInitAlloc(type, num)	\
	(type*) tmMemInitAlloc(num * sizeof(type))

// Allocate & Initialize Memory for ANSI String
#define	tmStrInitAllocA(size)	\
	(char*) tmMemInitAlloc(_AS(size))

// Allocate & Initialize Memory for UNICODE String
#define	tmStrInitAllocW(size)	\
	(WCHAR*) tmMemInitAlloc(_WS(size))

// Allocate & Initialize Memory for Text
#define tmStrInitAlloc(size)	\
	(TCHAR*) tmMemInitAlloc(_TS(size))

// Allocate & Initialize Memory for a Structure
#define	tmStructInitAlloc(type)	\
	(type*) tmMemInitAlloc(sizeof(type))

// ReAllocate Memory
#define tmMemReAlloc(mem, size)	\
	GlobalReAlloc(mem, size, 0)

// ReAllocate Memory for Array
#define	tmArrayReAlloc(arr, type, num)	\
	(type*) tmMemReAlloc(arr, num * sizeof(type))

// ReAllocate Memory for ANSI String
#define	tmStrReAllocA(str, len)	\
	(char*) tmMemReAlloc(str, _AS(len))

// ReAllocate Memory for UNICODE String
#define	tmStrReAllocW(str, len)	\
	(WCHAR*) tmMemReAlloc(str, _WS(len))
	
// ReAllocate Memory for Text
#define	tmStrReAlloc(str, len)	\
	(TCHAR*) tmMemReAlloc(str, _TS(len))

// ReAllocate Memory for a Structure
#define tmStructReAlloc(strct, type)	\
	(type*) tmMemReAlloc(strct, sizeof(type))

// ReAllocate & Initialize Memory
#define	tmMemInitReAlloc(mem, size)	\
	GlobalReAlloc(mem, size, GMEM_ZEROINIT)

// ReAllocate & Initialize Memory for Array
#define	tmArrayInitReAlloc(arr, type, num)	\
	(type*) tmMemInitReAlloc(arr, num * sizeof(type))

// ReAllocate & Initialize Memory for ANSI String
#define	tmStrInitReAllocA(str, len)	\
	(char*) tmMemInitReAlloc(str, _AS(len))

// ReAllocate & Initialize Memory for UNICODE String
#define	tmStrInitReAllocW(str, len)	\
	(WCHAR*) tmMemInitReAlloc(str, _WS(len))
	
// ReAllocate & Initialize Memory for Text
#define	tmStrInitReAlloc(str, len)	\
	(TCHAR*) tmMemInitReAlloc(str, _TS(len))

// ReAllocate & Initialize Memory for a Structure
#define tmStructInitReAlloc(strct, type)	\
	(type*) tmMemInitReAlloc(strct, sizeof(type))

// Free Memory
#define	tmMemFree(ptr)	\
	GlobalFree((HGLOBAL)ptr)

// Free Memory for Array
#define	tmArrayFree(ptr)	\
	tmMemFree(ptr)

// Free Memory for ANSI String
#define	tmStrFreeA(ptr)	\
	tmMemFree(ptr)

// Free Memory for UNICODE String
#define	tmStrFreeW(ptr)	\
	tmMemFree(ptr)
	
// Free Memory for Text
#define	tmStrFree(ptr)	\
	tmMemFree(ptr)

// Free Memory for a Structure
#define tmStructFree(ptr)	\
	tmMemFree(ptr)

// Initialize Memory
#define	tmMemInit(ptr, size)	\
	ZeroMemory(ptr, size)

// Initialize Memory for Array
#define	tmArrayInit(arr, type, num)	\
	tmMemInit(arr, num * sizeof(type))

// Initialize Memory for ANSI String
#define	tmStrInitA(str, len)	\
	tmMemInit(str, _AS(len))

// Initialize Memory for UNICODE String
#define	tmStrInitW(str, len)	\
	tmMemInit(str, _WS(len))
	
// Initialize Memory for Text
#define	tmStrInit(str, len)	\
	tmMemInit(str, _TS(len))

// Initialize Memory for a Structure
#define tmStructInit(strct, type)	\
	tmMemInit(strct, sizeof(type))




// Insert data to a Normal Queue (if possible)
#define		TM_INSERT_QUEUE1(queue, data)	\
	if(queue##.##Number < queue##.##Size)\
	{\
		queue##.##Data[queue##.##Rear] = data;\
		queue##.##Rear = (queue##.##Rear+1) % queue##.##Size;\
		queue##.##Number++;\
	}


// Insert data to a "Size2" Queue[queue whose size is a power of 2] (if possible)
#define		TM_INSERT_QUEUE2(queue, data)	\
	if(queue##.##Number < queue##.##Size)\
	{\
		queue##.##Data[queue##.##Rear] = data;\
		queue##.##Rear = (queue##.##Rear+1) & (queue##.##Size-1);\
		queue##.##Number++;\
	}


// Insert data to a Normal Buffered Queue (if possible)
#define		TM_INSERT_QUEUE1B(queue, data, buffering, datatype, uint_var)	\
	{\
		if(queue##.##Number < queue##.##Size)\
		{\
			TM_INSERT_QUEUE1(queue, data)\
		}\
		else\
		{\
			if((uint_var = (UINT) tmMemReAlloc(queue##.##Data, (queue##.##Size + buffering) * sizeof(datatype))))\
			{\
				queue##.##Data = (datatype*) uint_var;\
				for(uint_var = queue##.##Size - 1; uint_var >= queue##.##Front; uint_var--)\
					queue##.##Data[uint_var + buffering] = queue##.##Data[uint_var];\
				queue##.##Size += buffering;\
				TM_INSERT_QUEUE1(queue, data)\
			}\
		}\
	}


// Insert data to a "Size2" Buffered Queue[queue whose size is a power of 2] (if possible)
#define		TM_INSERT_QUEUE2B(queue, data, datatype, uint_var)	\
	{\
		if(queue##.##Number < queue##.##Size)\
		{\
			TM_INSERT_QUEUE2(queue, data)\
		}\
		else\
		{\
			if((uint_var = (UINT) tmMemReAlloc(queue##.##Data, (queue##.##Size << 1) * sizeof(datatype))))\
			{\
				queue##.##Data = (datatype*) uint_var;\
				for(uint_var = queue##.##Size-1; uint_var >= queue##.##Front; uint_var--)\
					queue##.##Data[uint_var + queue##.##Size] = queue##.##Data[uint_var];\
				queue##.##Size <<= 1;\
				TM_INSERT_QUEUE2(queue, data)\
			}\
		}\
	}


// Delete data from a Normal Queue (if possible)
#define		TM_DELETE_QUEUE1(queue, var)	\
	if(queue##..##Number)\
	{\
		var = queue##.##Data[queue##.##Front];\
		queue##.##Front = (queue##.##Front+1) % queue##.##Size;\
		queue##.##Number--;\
	}


// Delete data from a "Size2" Queue[queue whose size is a power of 2] (if possible)
#define		TM_DELETE_QUEUE2(queue, var)	\
	if(queue##.##Number)\
	{\
		var = queue##.##Data[queue##.##Front];\
		queue##.##Front = (queue##.##Front+1) & (queue##.##Size-1);\
		queue##.##Number--;\
	}


// Delete form a Normal Buffered Queue (if possible)
#define		TM_DELETE_QUEUE1B(queue, data, buffering, Datatype, uint_var)	\
	{\
		if(queue.Number <= (queue.Size - buffering))\
		{\
		}\
	}
// Delete data from a queue whose size must be a power of 2 ( max = size-1 )
// The macro may or may not be able to delete data from queue
// data may contain garbage value if queue is empty and data is not initialized
#define		TM_DELETE_QUEUE_BUFFERED(queue, var)	\
	if(queue.Number <= (queue.Size>>1))\
	{\
		queue.Size >>= 1;\
		if(queue.Front != (queue.Size-1))\
		{\
			\
		}\
	}\
	if(queue##.##Number)\
	{\
		var = queue##.##Data[queue##.##Front];\
		queue##.##Front = (queue##.##Front+1) & queue##.##Size;\
		queue##.##Number--;\
	}\
	if(queue


// Insert data to a Normal List (if possible)
#define		TM_INSERT_LIST1(list, data)	\
	if(list##.##Number < list##.##Size)\
	{\
		list##.##Data[list##.##Number] = data;\
		list##.##Number++;\
	}


// Insert data to a Buffered List (if possible)
#define		TM_INSERT_LIST1B(list, data, buffering, datatype, uint_var)	\
	TM_INSERT_LIST1(list, data)\
	else\
	{\
		if((uint_var = (UINT) tmMemReAlloc(list, (list##.##Size + buffering) * sizeof(datatype))))\
		{\
			list##.##Data = (datatype*) uint_var;\
			list##.##Size += buffering;\
			TM_INSERT_LIST1(list, data)\
		}\
	}


// Delete data from a Normal List (if possible)
#define		TM_DELETE_LIST1(list, index, uint_var)	\
	if(index < list##.##Number)\
	{\
		for(uint_var = index + 1; uint_var < list##.##Number; uint_var++)\
			list##.##Data[uint_var - 1] = list##.##Data[uint_var];\
		list##.##Number--;\
	}


// Delete data from a Buffered List (if possible)
#define		TM_DELETE_LIST1B(list, index, buffering, datatype, uint_var)	\
	{\
		if(list.Size - list.Number >= buffering)\
		{\
			if((uint_var = (UINT) tmMemReAlloc(list, (list##.##Size - buffering) * sizeof(datatype))))\
			{\
				list##.##Data = (datatype*) uint_var;\
				list##.##Size -= buffering;\
			}\
		}\
		TM_DELETE_LIST1(list, data)\
	}



#endif


