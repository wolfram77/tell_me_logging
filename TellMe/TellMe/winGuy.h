/****************************************************************************
 *                                                                          *
 * File    : winGuy.h                                                       *
 *                                                                          *
 * Purpose : Some Windows macros for C                                      *
 *                                                                          *
 * History : Date      Part of TellMe                                       *
 *           9/7/2011  Subhajit Sahu                                        *
 *                                                                          *
 ****************************************************************************/


#ifndef	_winGuy_h_
#define	_winGuy_h_		1

// ----Includes------------------------------------------------------
#define		WIN32_DEFAULT_LIBS
#include	<windows.h>
#include	<windowsx.h>
#include	<commctrl.h>
#include	<tchar.h>
#include	<windef.h>
#include	<wingdi.h>
#include	<gl/gl.h>
#include	<gl/glu.h>


// ----Basic Denns----------------------------------------------------
#define		NELEMS(a)																(sizeof(a) / sizeof((a)[0]))
#define		WPROC																	static LRESULT WINAPI
#define		_WPROC																	static LRESULT CALLBACK
#define		ENTRY																	int PASCAL
#define		PROC																	static int
#define		_HINSTANCE																HANDLE
#define		PROCESS_MSG(t,p)														case t: return(p(hWnd, Msg, wParam, lParam));
#define		_MAIN_PARAMS															HINSTANCE hInst, HINSTANCE hPrevInst, LPSTR lpszCmdLine, int nCmdShow


// ----Type Declarations---------------------------------------------

// 32-bit RGBA pixel
typedef struct _RGBA_PIXEL_DATA
	{
	unsigned char red;
	unsigned char green;
	unsigned char blue;
	unsigned char alpha;
	}RGBA_PIXEL_DATA;

// 32-bit RGB pixel for editing
typedef union _RGB_PIXEL
	{
	RGBA_PIXEL_DATA		clr;
	DWORD				value;
	}RGB_PIXEL;

// Message Data (for storage)
typedef struct _MSG_DATA
{
	UINT	Type;
	UINT	Param[3];
}MSG_DATA;




// ----Format Simplifiers--------------------------------------------
#define		_MSG_PARAMS	\
HWND hWnd, UINT Msg, WPARAM wParam, LPARAM lParam

#define		_MSG_PROTO_PARAMS	\
HWND, UINT, WPARAM, LPARAM

#define		_MSG_CALL_PARAMS	\
hWnd, Msg, wParam, lParam

#define		_MSG_ARRAY_TYPE		\
(*)(_MSG_PROTO_PARAMS)

#define	_VOID_CALL			void (*)()

#define	_LPMSG_CALL			void (*)(LPMSG)




// ----Macros--------------------------------------------------------

// Set a 32-bit RGB pixel
#define		_tmSetRGB_PIXEL(_RGBpixel, r, g, b)	\
_RGBpixel.clr.red=r;\
_RGBpixel.clr.green=g;\
_RGBpixel.clr.blue=b;\
_RGBpixel.clr.unused=0;


// Set a RECT
#define		_tmSetRECT(Rectangle, x1, y1, x2, y2)	\
Rectangle.top=y1;\
Rectangle.bottom=y2;\
Rectangle.left=x1;\
Rectangle.right=x2;



// Draw on a Device Context from a BITMAP object opaquely
#define		_tmBitBltOpaque(hdc, xdest, ydest, xres, yres, image, bitmap, xsrc, ysrc)		\
SelectObject(image, bitmap);\
BitBlt(hdc, xdest, ydest, xres, yres, image, xsrc, ysrc, SRCCOPY);


// Draw on a Device Context from a BITMAP object transparently
#define		_tmBitBltTrans(hdc, xdest, ydest, xres, yres, image, bitmap, mask, xsrc, ysrc)	\
SelectObject(image, mask);\
BitBlt(hdc, xdest, ydest, xres, yres, img, xsrc, ysrc, SRCAND);\
SelectObject(img, bitmap);\
BitBlt(hdc, xdest, ydest, xres, yres, image, xsrc, ysrc, SRCPAINT);





// ----Assembly Language Sets----------------------------------------


#define		_ASM_Stack_Push(var)	\
push	var

#define		_ASM_Stack_Pop(var)	\
pop		var

#define		_ASM_Stack_GetPtr(var)	\
mov		var, esp

#define		_ASM_Stack_SetPtr(var)	\
mov		esp, var

#define		_ASM_Stack_AddToPtr(var)	\
add		esp, var

#define		_ASM_Stack_SubtractFromPtr(var)	\
sub		esp, var

#define		_ASM_Stack_GetDataAtPtr(var)	\
mov		var, [esp]

#define		_ASM_Stack_SetDataAtPtr(var)	\
mov		[esp], var

#define		_ASM_Function_Call(func)	\
call	func










#endif

